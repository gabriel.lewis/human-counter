import cv2

def bbox_rel(*xyxy):
    """" Calculates the relative bounding box from absolute pixel values. """
    bbox_left = min([xyxy[0].item(), xyxy[2].item()])
    bbox_top = min([xyxy[1].item(), xyxy[3].item()])
    bbox_w = abs(xyxy[0].item() - xyxy[2].item())
    bbox_h = abs(xyxy[1].item() - xyxy[3].item())
    x_c = (bbox_left + bbox_w / 2)
    y_c = (bbox_top + bbox_h / 2)
    w = bbox_w
    h = bbox_h
    return x_c, y_c, w, h

def compute_color_for_labels(label):
    """
    Simple function that adds fixed color depending on the class
    """
    color = [int((p * (label ** 2 - label + 1)) % 255) for p in palette]
    return tuple(color)

def linear_equation(start, end):
    """
    Computes linear equation and returns gradient and y_icpt. 
    Returns none for both if vertical line and gradient of 0 if horizontal line.

    Parameters
    ----------
    start : A tuple containing x and y coordinates of the start of a line: (x,y)
    end : A tuple containing x and y coordinates of the end of a line: (x,y)

    Returns
    -------
    float: gradient
    float: y_icpt

    """
    #setting return values
    gradient = None
    y_icpt = None

    #gradient of line
    try:
        gradient = (start[1] - end[1])/(start[0] - end[0])
    except ZeroDivisionError:
        #handle vertical line
        gradient = "vertical"
        return gradient, y_icpt
        

    #handle horizontal line
    if gradient == 0:
        y_icpt = start[1]
        gradient = "horizontal"
        return gradient, y_icpt

    # handle other lines
    #find lower x
    if start[0] < end[0]:
        lower_x = start[0]
        y = start[1]
    elif end[0] < start[0]:
        lower_x = end[0]
        y = end[1]

    #find y_icpt
    y_icpt = y - (gradient * lower_x)
    return gradient, y_icpt

def draw_boxes(img, bbox, identities=None, offset=(0, 0)):
    for i, box in enumerate(bbox):
        x1, y1, x2, y2 = [int(i) for i in box]
        x1 += offset[0]
        x2 += offset[0]
        y1 += offset[1]
        y2 += offset[1]
        # box text and bar
        id = int(identities[i]) if identities is not None else 0
        color = compute_color_for_labels(id)
        label = '{}{:d}'.format("", id)
        t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 2, 2)[0]
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 3)
        cv2.rectangle(
            img, (x1, y1), (x1 + t_size[0] + 3, y1 + t_size[1] + 4), color, -1)
        cv2.putText(img, label, (x1, y1 +
                                 t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 2, [255, 255, 255], 2)
    return img