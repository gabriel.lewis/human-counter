version = "1.0"

train {
    step training {
        image = "tensorflow/tensorflow:2.2.0-gpu"
        install = [
            "apt-get update; apt-get install ffmpeg libsm6 libxext6 -y", //opencv
            "apt-get install -y git", //to extract git hashes
            "pip3 install --upgrade pip",
            "pip3 install Cython==0.29.17 numpy==1.19.2",
            "pip3 install -r yolov5/requirements_train.txt",
        ]
        script = [{sh = ["python smart_train.py"]}]
        resources {
            cpu = "2"
            memory = "12G"
            gpu = "1"
        }
    }

    parameters {
        NUM_EPOCHS = "10"
        BATCH_SIZE = "4"
    }

    secrets = [
        "AWS_SECRET_ACCESS_KEY",
        "AWS_ACCESS_KEY_ID"
    ]
}

serve {
    image = "pytorch/pytorch:1.5.1-cuda10.1-cudnn7-runtime"
    install = [
        "apt-get update; apt-get install ffmpeg libsm6 libxext6 -y", //opencv
        "apt-get install -y git", //to extract git hashes
        "pip install Cython==0.29.17 numpy==1.19.2",
        "pip install -r requirements.txt",
    ]
    script = [
        {sh = [
            "streamlit run app.py --server.headless=true --server.port=${BEDROCK_SERVER_PORT} --browser.serverAddress=0.0.0.0"
        ]}
    ]
    parameters {
        WORKERS = "1"
    }
}



