# Human Counter

## Introduction

There are many use cases for apps that count humans, cars and other common mobile entities (henceforth termed 'mobs'). This script is an implementation of yolov5, deepsort and a object counter that can be used to keep track of the number of mobs. Yolov5 is pretrained on the [COCO dataset] (https://cocodataset.org/#home). Should you need to train it on new mobs, you may use the train function as with yolov5.

## Usage

### Streamlit app
The streamlit app allows for users to upload a video file, annotate a line directly on a frame of the video you want to count mobs passing through, and obtain either a downloadable video annotated with mob centroids or have the annotated video displayed frame by frame on the web app itself. Use instructions are listed in the app page itself.

### Worker script
The worker script allows users to use this functionality in the command line
```
worker.py [--view-img DISPLAY-OUTPUT]
          [--source SOURCE]
          [--line_style {"horizontal", "vertical"}]

# e.g run worker from webcam and display streamed annotated output
python worker.py --view-img=True --source=0

# e.g run worker from local video file and display streamed annotated output and define line (e.g horizontal line through the center of image)
python worker.py --view-img=True --source=./video_file.mp4 --line_style("horizontal")