import argparse
import os
import shutil
import sys
import time
import yaml

import matplotlib.pyplot as plt
import numpy as np
from numpy import random
import cv2
import yaml
import torch

cur_dir = os.getcwd()
sys.path.append(cur_dir + "/src")

from models.experimental import attempt_load
from utils.general import check_img_size, non_max_suppression, plot_one_box, scale_coords, set_logging, xyxy2xywh
from utils.torch_utils import select_device, time_synchronized


def load_model(num_classes, weights_path, opt, imgsz):
    set_logging()
    try:
        out = opt.output
        device = select_device(opt.device)
    except:
        out = opt
        device = select_device("")
    
    if os.path.exists(out):
        shutil.rmtree(out)
    os.makedirs(out)
    half = device.type != "cpu"  # half precision only supported on CUDA
    
    # Load model
    model = torch.load(weights_path, map_location=device)['model'].float().fuse().eval()
    
    imgsz = check_img_size(imgsz, s=model.stride.max())  # check img_size
    if half:
        model.half()  # to FP16
    
    names = model.module.names if hasattr(model, "module") else model.names
    
    random.seed(678)
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]
    
    return model, half, device, imgsz, names, colors



# Initialize
parser = argparse.ArgumentParser()
# opt = parser.parse_args()
opt, _ = parser.parse_known_args()

opt.conf_thres = 0.30    # score_th
opt.iou_thres = 0.50     # nms_iou
opt.device = ""
opt.classes = None
opt.agnostic_nms = False
opt.augment = False
opt.update = False
opt.output = "./logs/inference/output"
view_img = False
save_txt = False
imgsz = 640
save_img = True

with open("./conf/custom_5m.yaml", "r") as f:
    config = yaml.load(f)
mapper = dict([(name,int(i)) for i, name in enumerate(config['names'])])

class_mapper = {v: k for k, v in mapper.items()}

num_classes = len(mapper)

# autodetect bedrock environment
# if os.environ.get("BEDROCK_POD_NAME"): 
#     weights_path = "/artefact/best.pt"
# else: 
#     weights_path = "data/07_model_output/best.pt"
weights_path = "data/07_model_output/best.pt"

print("Using %s." % weights_path)

model, half, device, imgsz, names_, colors = load_model(num_classes, weights_path, opt, imgsz)

def detect(original_image, score_th, nms_iou, w_th=0.03, h_th=0.03, maxFeatures=20, takemodel=None):
    """
    Perform inference on image
    Args:
        original_image (np.array): Raw RGB image
        score_th (float): minimum score threshold
        nms_iou (float): IOU for bbox to be considered belonging to same obj
        maxFeatures (int): keep only top N most confident predicted bbox
    """
    H, W = original_image.shape[:2]
    img, im0s = preprocess(original_image, check=False)
    
    im0 = np.ascontiguousarray(im0s)
    gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh

    img = torch.from_numpy(img).to(device)

    img = img.half() if half else img.float()  # uint8 to fp16/32
    img /= 255.0  # 0 - 255 to 0.0 - 1.0
    if img.ndimension() == 3:
        img = img.unsqueeze(0)
    
    # Inference
    if takemodel is None:
        pred = model(img, augment=False)[0]
        # print("pred: ", pred)
        # print("pred.shape: ", pred.shape)
    else:
        pred = takemodel(img, augment=False)[0]
    
    # Apply NMS
    pred = non_max_suppression(pred, score_th, nms_iou, classes=opt.classes, agnostic=False)
    # print("\n After non-max suppression")
    # print("pred: ", pred[0])
    # print("Sleeping for 2 seconds....")
    # time.sleep(2)
    
    # Process detections
    for i, det in enumerate(pred):  # detections per image
        holder = []
        
        if det is not None and len(det):
            # Rescale boxes from img_size to im0 size
            det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()
            det = det.cpu().detach().numpy()
            
            w = (det[:,2]-det[:,0])/W
            h = (det[:,3]-det[:,1])/H
            keep = np.logical_and((w > w_th), (h > h_th))
            det = det[keep, :]
            
            # # Write results
            for *xyxy, conf, cls_id in reversed(det):
                if cls_id == 0:
                    holder.append([*xyxy, conf, cls_id])

    xyxysc = np.array(holder)
    
    if len(xyxysc) == 0:
        return np.array([]), np.ascontiguousarray(im0s)
    else:
        xyxysc = xyxysc[(-xyxysc[:,4]).argsort()][:maxFeatures, :]
        return xyxysc, np.ascontiguousarray(im0s)
    


def show_bbox(image, xyxysc, names, colors, save=False, display=True):
    """
    Args:
        image (np.array): Raw RGB image
        xyxysc (np.array): Predicted bbox array of shape (N, 6)
        names (list): Category names as string
    """
    for *xyxy, conf, cls_id in xyxysc:
        label = "%s %.2f" % (names[int(cls_id)], conf)
        plot_one_box(xyxy, image, label=label, color=colors[int(cls_id)], line_thickness=2)
    
    if display:
        plt.imshow(image)
        plt.axis('off')  ###
        plt.show()
    
    if save:
        filename = os.path.join(opt.output, "%d.jpg"%int(100*time.time()%10**6))
        cv2.imwrite(filename, image[:,:,::-1])
    
    return image



def conver_xyxy_into_yolo(xyxysc, H, W):
    """
    Args:
        xyxysc (np.array): x1, y1, x2, y2, score, class_id
        H (int): image height
        W (int): image width
    Return:
        xcycwhsc (np.array): normalized xc, yc, w, h
    """
    if len(xyxysc)==0:
        return np.array([])
    
    xcycwhsc = []
    xc_top, yc_top = (xyxysc[:,0]+xyxysc[:,2])/(2*W), (xyxysc[:,1]+xyxysc[:,3])/(2*H)
    w_top,  h_top  = (xyxysc[:,2]-xyxysc[:,0])/W,     (xyxysc[:,3]-xyxysc[:,1])/H
    
    for x, y, w, h, s, cid in zip(xc_top, yc_top, w_top, h_top, xyxysc[:,4], xyxysc[:,5]):
        xcycwhsc.append([x,y,w,h,s,cid])

    return np.array(xcycwhsc)




def conver_arr_into_json(pred_arr, class_mapper):
    """
    To convert array output from `detect` function into a json format used in SU1
    Args:
        pred_arr (array): Predictions correspond to xc, yc, w, h, score, class_id
        class_mapper (dict): dictionary where key is class_id (as int) and value is obj_name
    """
    dictionary = {
        key: {"boundingPoly" : {"normalizedVertices": []},
              "name": key
              }
        for key in class_mapper.values()
    }
    
    for x, y, w, h, s, cid in pred_arr:
        box = {'x':float(x), 'y':float(y), 'width':float(w), 'height':float(h), 'score':float(s)}
        key = class_mapper[cid]
        dictionary[key]["boundingPoly"]["normalizedVertices"].append(box) 
    
    return dictionary



def letterbox(img, new_shape=(640, 640), color=(114, 114, 114), auto=True, scaleFill=False, scaleup=True):
    # Resize image to a 32-pixel-multiple rectangle https://github.com/ultralytics/yolov3/issues/232
    shape = img.shape[:2]  # current shape [height, width]
    if isinstance(new_shape, int):
        new_shape = (new_shape, new_shape)

    # Scale ratio (new / old)
    r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
    if not scaleup:  # only scale down, do not scale up (for better test mAP)
        r = min(r, 1.0)

    # Compute padding
    ratio = r, r  # width, height ratios
    new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
    dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]  # wh padding
    if auto:  # minimum rectangle
        dw, dh = np.mod(dw, 64), np.mod(dh, 64)  # wh padding
    elif scaleFill:  # stretch
        dw, dh = 0.0, 0.0
        new_unpad = (new_shape[1], new_shape[0])
        ratio = new_shape[1] / shape[1], new_shape[0] / shape[0]  # width, height ratios

    dw /= 2  # divide padding into 2 sides
    dh /= 2

    if shape[::-1] != new_unpad:  # resize
        img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)
    top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
    left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
    img = cv2.copyMakeBorder(
        img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color
    )  # add border
    return img, ratio, (dw, dh)



def preprocess(img0, check=False):
    new_shape = 640

    # Padded resize
    img = letterbox(img0, new_shape)[0]

    # Convert
    img = img.transpose(2, 0, 1)  # RGB, to 3x416x416
    img = np.ascontiguousarray(img)

    if check is True:
        plt.figure(figsize=(8, 6))
        plt.subplot(2, 1, 1)
        plt.imshow(img0)
        plt.title("original image")
        plt.subplot(2, 1, 2)
        plt.imshow(np.transpose(img, (1, 2, 0)))
        plt.title("padded image")
        plt.show()

    return img, img0





if __name__ == "__main__":
    
    #obj_category = "wheelchair"
    #img_folder = "/home/james/Desktop/desk_holder/su2_all_class/%s/test/images" % obj_category
    img_folder = "./data/test_image"
    list_of_images = [os.path.join(img_folder,file) for file in os.listdir(img_folder)]
    
    with torch.no_grad():
        img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
        
        _ = model(img.half() if half else img) if device.type != "cpu" else None   # run once
        
        for path in list_of_images:
            original_image = np.ascontiguousarray(cv2.imread(path)[:, :, ::-1])

            pred_arr, _ = detect(original_image, 
                score_th = opt.conf_thres, 
                nms_iou = opt.iou_thres, 
                w_th=0.03, 
                h_th=0.03,
            )
            
            H, W = original_image.shape[:2]
            
            xyxysc = pred_arr
            marked_img = show_bbox(original_image, 
                xyxysc, 
                config["names"], 
                colors, 
                save=True
            )
            
            xcycwhsc = conver_xyxy_into_yolo(pred_arr, H, W)
            output_arr = conver_arr_into_json(xcycwhsc, class_mapper)
    
    if save_img:
        print("Results saved to %s" % os.path.abspath(opt.output))
