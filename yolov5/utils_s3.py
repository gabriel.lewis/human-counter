
import os
import sys
import yaml
import subprocess



def download_weights(folder, file):
    """
    Args:
        folder (str): can have intermediate `/` (but no leading or trailing)
        file (str): include extension (eg. yolov5s.pt)
    """
    cmd = """
        aws s3 cp s3://{}/{} ./data/06_models/{}
        """.format(folder, file, file)
    process = subprocess.Popen(['/bin/bash', '-c', cmd])
    _, _ = process.communicate()
    

def download_config(folder, file):
    """
    Args:
        folder (str): can have intermediate `/` (but no leading or trailing)
        file (str): include extension (eg. custom_5m.yaml)
    """
    cmd = """
        aws s3 cp s3://{}/{} ./conf/{}
        """.format(folder, file, file)
    process = subprocess.Popen(['/bin/bash', '-c', cmd])
    _, _ = process.communicate()
    