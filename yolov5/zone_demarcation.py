
import cv2
import numpy as np
import matplotlib.pyplot as plt
from math import pi as pi
from scipy.spatial import ConvexHull

from predict import detect


class Hull():
    def __init__(self):
        print("Initialised Hull object")
    

    def points_from_bbox_arr(self, pred_array, method='proj_floor'):
        """
        Args:
            pred_array (np.array): shape (nrows, 6), where columns are (x1,y1,x2,y2,conf,class_id) 
            method (str): 'all'        -> take all four corners of the bounding box
                          'bottom'     -> take only bottom two corners of bounding box
                          'proj_floor' -> project 4 points on the floor containing the object
        """
        self.Points_dict = {}
        self.holder      = []
        
        pred_array = pred_array.astype(np.int16)
        
        for i, row in enumerate(pred_array):
            self.Points_dict[i] = {'a': (row[0],row[1]), 
                                   'b': (row[2],row[3])
                                   }
            x1, y1, x2, y2 = row[0], row[1], row[2], row[3]
            
            if (method == 'all'):
                self.holder.extend([(x1,y1),(x2,y1),(x2,y2),(x1,y2)])
            elif (method == 'bottom'):
                self.holder.extend([(x2,y2),(x1,y2)])
            elif (method == 'proj_floor'):
                a = 0.2*(y2-y1)   # 20% of 2D bounding box height
                self.holder.extend([(x1,y2-a),(x2,y2-a),(x2,y2),(x1,y2)])
            else:
                assert False, 'Please ensure method is either all | bottom | proj_floor'
    
    
    def get_hull(self, image):
        self.points = np.array(self.holder)
        
        if len(self.points) == 0:
            print("No cones detected in the image")
        else:
            self.hull = ConvexHull(self.points)
            self.draw_bbox_and_hull(image)
    
    
    def draw_bbox_and_hull(self, image):
        """
        Arg:
            image (np.array): RGB image of shape (H,W,3)
        """
        bbox_color = (255,0,0)
        
        for key, val in self.Points_dict.items():
            cv2.rectangle(image, val['a'], val['b'], bbox_color, 1)
    
        plt.imshow(image)
        
        plt.plot(self.points[:,0], self.points[:,1], 'o')
        for simplex in self.hull.simplices:
            plt.plot(self.points[simplex, 0], self.points[simplex, 1], 'r-')
        
        plt.show()
        



class Zone_interpreter():
    def __init__(self, AgentH):
        print("Initialised class")
        self.AgentH = AgentH
        
    
    def run_det_and_draw(self, image_path):
        """
        Makes prediction (on single view) and returns json conclusion & marked image
        """
        imgarr = cv2.imread(image_path)[:,:,::-1]

        pred_arr, _ = detect(imgarr, 
                             score_th = 0.30, 
                             nms_iou = 0.40, 
                             w_th=0.03, 
                             h_th=0.03,
                             )
        
        return pred_arr



    def get_zone_from_image(self, image_path, camera_info, coneid=2):
        pred_array = self.run_det_and_draw(image_path)
        
        image      = cv2.imread(image_path)
        image      = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        
        self.AgentH.points_from_bbox_arr(pred_array[pred_array[:,-1]==coneid, ], method='proj_floor')
        self.AgentH.get_hull(image)
        
        self.get_loc_from_hull(image, camera_info)
        
        


    def get_loc_from_hull(self, image, camera_info):
        """
        Requires 2D image, predicted no-go-zone on 2D image, and camera specs info
        Output estimated span of no-go-zone (eg. starts 1m ahead, ends 3m ahead, zone spreads 3m wide)
        """
        points_within_hull = self.AgentH.points      # np.array of shape (N, 2), where N = 4*num_of_predicted_bbox
        if len(points_within_hull) == 0:
            return None
        
        top_in_image   = np.min(points_within_hull[:,1])   # position of hull's top, in number of pixels from top of 2D image
        bot_in_image   = np.max(points_within_hull[:,1])   #             hull's bottom
        left_in_image  = np.min(points_within_hull[:,0])   #             hull's left
        right_in_image = np.max(points_within_hull[:,0])   #             hull's right
        
        H_image, W_image, _ = image.shape
        
        beta_down = camera_info['down']
        beta_left = camera_info['left']
        
        pointofinterest = self.AgentH.points[:,:]
        
        location = self.convert_pixel_to_loc(H_image, W_image, pointofinterest, camera_info)
        print("location: \n", location)
        
        z_far   = np.max(location[:,2])
        z_near  = np.min(location[:,2])
        x_left  = np.min(location[:,0])
        x_right = np.max(location[:,0])
        
        dist_from_cam = {'far'  : z_far,
                         'near' : z_near,
                         'left' : x_left,
                         'right': x_right,
                         }
        print("dist_from_cam: \n", dist_from_cam)
        return location
    
    
    def convert_pixel_to_loc(self, H_image, W_image, pixel_arr, camera_info):
        """
        Conversion done in parallel for all points
        Convert pixel coords (u,v) to camera coords (x_cam, y_cam, z_cam) to robot frame (x,y,z)
        Args:
            H_image (int): height of image in pixels
            W_image (int): width of image in pixels
            pixel_arr (np.array): shape (N,2), where N is number of points and two columns correspond to pixel locations (h,w) on the 2D image
        Return:
            location (np.array): shape (N,2), where three columns correspond to real position (x,y,z) in robot's frame of reference
        """
        print("pixel_arr: ", pixel_arr.shape)
        pixel_arr = pixel_arr.reshape(-1,2)
        
        u = pixel_arr[:,1]   #pixel_arr[:,0]    # number of pixels from top of 2D image
        v = pixel_arr[:,0]   #pixel_arr[:,1]    # number of pixels from left of 2D image
        print("u: ", u)
        print("v: ", v)
        
        alpha_bot = 90 - camera_info['down'] - 0.5*camera_info['FOV']   # angle (in degrees) wrt true vertical from camera to ground
        
        z_cam = camera_info['y_cam'] * np.tan((alpha_bot + ((H_image-u)/H_image)*camera_info['FOV'])*pi/180)
        # z_cam is coordinates in camera frame, NOT the location of camera itself
        
        x_cam = z_cam * np.tan((v-W_image/2)/W_image*camera_info['FOV']*pi/180)
        
        x, y, z = x_cam, np.zeros_like(x_cam), z_cam
        
        return np.stack([x,y,z], axis=1)







if __name__ == '__main__':
    image_path = "./data/test_image/cone_dsl_00091.jpg"
    
    camera_info = {'y_cam' : 1.50,    # height of camera above ground
                   'down'  : 40,      # in degrees downwards, centre projection wrt true horizontal
                   'left'  : 0,       # in degrees leftward, centre projection wrt global z-axis
                   'fstop' : 1.7,     # ratio of focal length to aperture diameter
                   'FOV'   : 80,      # in degrees
                   'sensor': 1/2.8,   # in inches
                   }
     
    AgentH = Hull()
    AgentZ = Zone_interpreter(AgentH)
    
    AgentZ.get_zone_from_image(image_path, camera_info) 