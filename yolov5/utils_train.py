
import os
import sys
import yaml
import subprocess


def get_file_list(folder, file_extensions):
    """
    get list of files from their extensions
    Args
        folder (str): folder path
        file_extensions (tuple): file extensions to search
            eg. (".jpg", "jpeg", ".png")
    Return
        list of file names
    """
    file_list = []
    for file in os.listdir(folder):
        if file.endswith(file_extensions):
            file_list.append(file)
    return sorted(file_list)



def folder_naming_convention(data_dir, setname):
    """
    define naming convention for train & validation folders
    """
    train_img_dir = os.path.join(data_dir, "images/%s_train" % setname)
    train_label_dir = os.path.join(data_dir, "labels/%s_train" % setname)
    val_img_dir = os.path.join(data_dir, "images/%s_test" % setname)
    val_label_dir = os.path.join(data_dir, "labels/%s_test" % setname)
    
    return train_img_dir, train_label_dir, val_img_dir, val_label_dir




def compare_folder_contents(folderA, folderB):
    """
    Check whether contents (ignore extension) in folderA and folderB are identical
    """
    filesA = set([('.').join(file.split('.')[:-1]) for file in os.listdir(folderA)])
    filesB = set([('.').join(file.split('.')[:-1]) for file in os.listdir(folderB)])
    if len(filesA.difference(filesB)) > 0:
        print("Files present only in folderA: ", filesA.difference(filesB))
    if len(filesB.difference(filesA)) > 0:
        print("Files present only in folderB: ", filesB.difference(filesA))


def download_data(folders, bucket=None):
    """
    Args:
        folders: List of strings. Each string is has images/labels folder in s3://vama-sceneuds-images/SU21corpus
    """
    if bucket is None:
        bucket = "vama-sceneuds-images/SU21corpus"
    
    print("+++ Remember to set AWS_SECRET_ACCESS_KEY and AWS_SECRET_ACCESS_KEY for %s" % bucket)
    for folder_name in folders:
        print("Downloading from s3://{}/images/{}".format(bucket, folder_name))
        # os.system("aws s3 sync s3://{}/images/{} ./data/01_raw/images/{}/;".format(bucket, folder_name, folder_name))
        # os.system("aws s3 sync s3://{}/labels/{} ./data/01_raw/labels/{}/;".format(bucket, folder_name, folder_name))
        cmd = """
            aws s3 sync s3://{}/images/{} ./data/01_raw/images/{}/;
            aws s3 sync s3://{}/labels/{} ./data/01_raw/labels/{}/;
            """.format(bucket, folder_name, folder_name, bucket, folder_name, folder_name)
        process = subprocess.Popen(['/bin/bash', '-c', cmd])
        _, _ = process.communicate()
        status = process.returncode
        
        if status != 0:
            print("Unable to sync with AWS for %s/%s" % (bucket, folder_name))
            sys.exit(1)
        else:
            print("Completed syncing with AWS for %s/%s" % (bucket, folder_name))
            compare_folder_contents("./data/01_raw/images/{}".format(folder_name), "./data/01_raw/images/{}".format(folder_name))



# def update_config(trainfolder, testfolder, class_list=None):
#     """
#     Args:
#         trainfolder (str): Name of folder for train set
#         testfolder (str): Name of folder for test set
#         class_list: list of string (if none, will not make changes)
#     """
#     configpath = "./conf/custom_5m.yaml"
#     with open(configpath, "r") as f:
#         data_loaded = yaml.safe_load(f)
    
#     data_loaded["train"] = ("/").join(data_loaded["train"].split("/")[:-2]) + "/%s/"%trainfolder
#     data_loaded["val"] = ("/").join(data_loaded["val"].split("/")[:-2]) + "/%s/"%testfolder
#     if class_list is not None:
#         data_loaded["names"] = class_list
#         data_loaded["nc"] = len(class_list)
    
#     with open(configpath, 'w') as f:
#         yaml.dump(data_loaded, f, default_flow_style=None)

def update_config(modeltype, trainfolder, testfolder, class_list):
    """
    Args:
        modeltype (str): s/m/l/x
        trainfolder (str): Name of folder for train set
        testfolder (str): Name of folder for test set
    """
    configpath = "./conf/custom_5%s.yaml" % modeltype
    
    with open(configpath, "r") as f:
        data_loaded = yaml.safe_load(f)
    
    data_loaded["train"] = ("/").join(data_loaded["train"].split("/")[:4]) + "/%s/"%trainfolder
    data_loaded["val"] = ("/").join(data_loaded["val"].split("/")[:4]) + "/%s/"%testfolder
    data_loaded["names"] = class_list
    data_loaded["nc"] = len(class_list)
    
    with open(configpath, 'w') as f:
        yaml.dump(data_loaded, f, default_flow_style=None)

